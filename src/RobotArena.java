import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;

import java.util.ArrayList;

public class RobotArena {
    public ArrayList<ArenaItem> getArenaItems() {
        return arenaItems;
    }

    public ArrayList<ArenaItem> arenaItems;

    public RobotArena() {
        arenaItems = new ArrayList<>();
    }

    public void addArenaItem(ArenaItem item) {
        if (!hasCollision(item)) {
            arenaItems.add(item);
        }else{
            System.out.println("£££££££");
        }
    }

    private boolean hasCollision(ArenaItem newItem) {
        for (ArenaItem existingItem : arenaItems) {
            if (checkCollision(newItem, existingItem)) {
                return true; // Collision detected
            }
        }
        return false; // No collision
    }

    private boolean checkCollision(ArenaItem item1, ArenaItem item2) {
        double distance = Math.sqrt(Math.pow(item1.getX() - item2.getX(), 2) + Math.pow(item1.getY() - item2.getY(), 2));
        double combinedRadius = item1.getBodyRadius() + item2.getBodyRadius()+10;

        return distance < combinedRadius;
    }
    public void handleAddAntennae(BorderPane borderPane) {
        // Generate random parameters
        if (borderPane == null) {
            System.err.println("BorderPane is null");
            return;
        }
        double bodyRadius = 20; // Body radius
        double x = Math.random() * (borderPane.getWidth() - 2 * bodyRadius - 60) + bodyRadius;
        double y = Math.random() * (borderPane.getHeight() - 2 * bodyRadius - 60 - 20) + bodyRadius;

        Color bodyColor = Color.color(Math.random(), Math.random(), Math.random());  // Random RGB color
        Color wheelColor = Color.color(Math.random(), Math.random(), Math.random());  // Random RGB color
        int speed = (int) (Math.random() * 2) + 1;  // Random speed in the range [1, 5]

        // Create RobotWithAntennae with random parameters
        Robot robot = new RobotWithAntennae(this,x, y, bodyRadius, bodyColor, wheelColor, speed);

        // Add the robot to the arena
        this.addArenaItem(robot);
    }
    public void handleAddRobot(BorderPane borderPane) {
        // Generate random parameters
        if (borderPane == null) {
            System.err.println("BorderPane is null");
            return;
        }
        double bodyRadius = 20; // Body radius
        double x = Math.random() * (borderPane.getWidth() - 2 * bodyRadius - 60) + bodyRadius;
        double y = Math.random() * (borderPane.getHeight() - 2 * bodyRadius - 60 - 20) + bodyRadius;

        Color bodyColor = Color.color(Math.random(), Math.random(), Math.random());  // Random RGB color
        Color wheelColor = Color.color(Math.random(), Math.random(), Math.random());  // Random RGB color
        int speed = (int) (Math.random() * 2) + 1;  // Random speed in the range [1, 5]

        // Create Robot with random parameters
        Robot robot = new Robot(this, x, y, bodyRadius, bodyColor, wheelColor, speed) {

        };

        // Add the robot to the arena
        this.addArenaItem(robot);
    }
}

