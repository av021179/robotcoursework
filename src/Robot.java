import javafx.animation.AnimationTimer;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

public abstract class Robot extends ArenaItem {

    private Color bodyColor;
    private Color wheelColor;
    private Circle body;
    private Rectangle leftWheel;
    private Rectangle rightWheel;
    private RobotArena robotArena;
    protected static boolean continueMoving = true;
    public double speed;
    private boolean isMoving = false;
    private static int nextId = 1;
    private int robotId;
    private AnimationTimer timer;
    private double rotationAngle;
    public Robot(RobotArena robotArena,double x, double y, double bodyRadius, Color bodyColor, Color wheelColor, double speed) {
        this.x = x;
        this.y = y;
        this.bodyRadius = bodyRadius;
        this.bodyColor = bodyColor;
        this.wheelColor = wheelColor;
        this.speed = speed;


        this.robotArena = robotArena;

        createRobot();
        updateView();
       rotationAngle = Math.random() * 360.0;
        setRotate(rotationAngle);
    }




    private void createRobot() {
        body = new Circle(bodyRadius, bodyColor);

        double wheelWidth = bodyRadius * 0.215;
        double wheelHeight = bodyRadius * 1.5;

        leftWheel = new Rectangle(-bodyRadius, -wheelHeight / 2, wheelWidth, wheelHeight);
        leftWheel.setArcWidth(10);
        leftWheel.setArcHeight(10);
        leftWheel.setFill(wheelColor);

        rightWheel = new Rectangle(bodyRadius - wheelWidth, -wheelHeight / 2, wheelWidth, wheelHeight);
        rightWheel.setArcWidth(10);
        rightWheel.setArcHeight(10);
        rightWheel.setFill(wheelColor);


        getChildren().addAll(body, leftWheel, rightWheel);
    }

    public void updatePosition(double x, double y) {
        this.x = x;
        this.y = y;
        rotateRobot();

        updateView();
    }

    protected void updateView() {
        setTranslateX(this.getX());
        setTranslateY(this.getY());
        rotateRobot();


    }

    // Add a method to handle the forward movement until intersection


    public void stopMoving() {
        isMoving = false;
    }

    // Add a method to resume the forward movement
    public void resumeMoving() {
        isMoving = true;
    }

    // Add a method to rotate the robot to the left


    // Add a method to rotate the robot to the right
    public void turnVertical() {
        normalizeRotationAngle();

            rotationAngle =-rotationAngle; // Adjust the rotation angle

    }
    public void turnHorizontal() {
        normalizeRotationAngle();

        rotationAngle =180-rotationAngle; // Adjust the rotation angle

    }

    // Add a method to normalize the rotation angle
    private void normalizeRotationAngle() {
        if (rotationAngle < 0) {
            rotationAngle = (rotationAngle % 360) + 360;
        } else if (rotationAngle >= 360) {
            rotationAngle %= 360;
        }
        setRotate(rotationAngle);
    }

    // Add a method to handle the forward movement





    // Add a method to rotate the robot
    private void rotateRobot() {
        setRotate(rotationAngle); // Your rotation logic here
    }


    // You can add more methods for checking intersection with other objects
    private void moveRobotForward() {
        double moveDistance = this.speed; // Adjust the distance to move in each step

        // Create a new AnimationTimer each time moveRobotForward is called
        timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                if (!continueMoving) {
                    stopMoving();
                    return;
                }
                double canvasWidth = getScene().getWidth();
                double canvasHeight = getScene().getHeight();
                // Rotate the robot
                setRotate(rotationAngle);

                // Convert rotation angle to radians
                double radianAngle = Math.toRadians(rotationAngle);
                setRotate(rotationAngle);
                // Calculate the movement direction based on the current rotation angle
                double deltaX = Math.cos(radianAngle) * moveDistance;
                double deltaY = Math.sin(radianAngle) * moveDistance;
                setRotate(rotationAngle);
                // Check for intersection with the canvas border
                if (getX() + deltaX - getBodyRadius() < 0 || getX() + deltaX + getBodyRadius() > canvasWidth) {
                    // Handle collision with left or right border
                    stopMoving(); // Stop the robot
                    turnHorizontal(); // Adjust the rotation angle
                    resumeMoving(); // Resume the robot's movement
                } else if (getY() - deltaY - getBodyRadius() < 0 || getY() - deltaY + getBodyRadius() > canvasHeight - 60) {
                    // Check for collision with top or bottom border
                    // Handle collision with top or bottom border
                    stopMoving(); // Stop the robot
                    turnVertical(); // Adjust the rotation angle
                    resumeMoving(); // Resume the robot's movement
                } else {
                    // Check for collision with other objects
                    boolean hasCollision = false;
                    for (ArenaItem arenaItem : robotArena.getArenaItems()) {
                        if (arenaItem != Robot.this && arenaItem.isIntersecting(getX() + deltaX, getY() - deltaY, getBodyRadius())) {
                            hasCollision = true;
                            calculateCollisionAngle(getX() + deltaX, getY() - deltaY, getBodyRadius());
                            break;
                        }
                    }

                    if (hasCollision) {
                        // Handle collision with other objects
                        stopMoving(); // Stop the robot

                        resumeMoving(); // Resume the robot's movement
                    } else {
                        // Move the robot forward in the right direction
                        updatePosition(getX() + deltaX, getY() - deltaY);
                        rotateRobot();
                    }
                }
            }
        };

        timer.start(); // Start the AnimationTimer
    }


    @Override
    public void startMoving() {
        if (!isMoving) {
            isMoving = true;
            timer=null;
            moveRobotForward();
        }
    }
    public boolean isIntersecting(double testX, double testY, double testRadius) {
        double distance = Math.sqrt(Math.pow(testX - getX(), 2) + Math.pow(testY - getY(), 2));
        double combinedRadius = getBodyRadius() + testRadius;

        return distance < combinedRadius;
    }
    @Override
    public String toString() {
        return "Robot{" +
                "name='" + getClass().getSimpleName() + '\'' +
                ", id=" + robotId +
                ", position=(" + getX() + ", " + getY() + ")  " +getRotate()+
                '}';
    }
    public void calculateCollisionAngle(double x1, double y1, double radius1) {
        double Xcoords = this.getX() - x1;
        double Ycoords = this.getY() - y1;

        double angleRadians = Math.atan2(Ycoords, Xcoords);


        double angleDegrees = Math.toDegrees(angleRadians);


        rotationAngle = -angleDegrees;

    }
}
