import javafx.scene.Group;

public abstract class ArenaItem extends Group {
    public double x; // x-coordinate
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double y; // y-coordinate

    public double getBodyRadius() {
        return bodyRadius;
    }

    public void setBodyRadius(double bodyRadius) {
        this.bodyRadius = bodyRadius;
    }

    public double bodyRadius;
    public abstract void startMoving();
    public abstract String toString();

    public abstract void stopMoving();

    public abstract boolean isIntersecting(double v, double v1, double bodyRadius);
}
