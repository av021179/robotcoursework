import javafx.animation.AnimationTimer;
import javafx.scene.paint.Color;

public class RobotWithAntennae extends Robot {

    private CustomLine leftAntenna;
    private CustomLine rightAntenna;
    private RobotArena robotArena;
    private boolean isMoving = false;
    private double rotationAngle = 0.0;
    private AnimationTimer timer;
    public RobotWithAntennae(RobotArena robotArena,double x, double y, double bodyRadius, Color bodyColor, Color wheelColor, double speed) {
        super(robotArena,x, y, bodyRadius, bodyColor, wheelColor, speed);

        // Set a random initial direction
        rotationAngle = Math.random() * 360.0;
        setRotate(rotationAngle);
            this.robotArena = robotArena;
            createAntennae();
        }






    private void createAntennae() {
        double antennaLength = getBodyRadius() * 0.8; // Adjust the length of the antennae as needed

        // Create antennae using CustomLine
        leftAntenna = new CustomLine(-getBodyRadius()*0.47, -getBodyRadius() * 0.9, -getBodyRadius()*0.4 - antennaLength, -antennaLength * 3);
        rightAntenna = new CustomLine(getBodyRadius()*0.47, -getBodyRadius() * 0.9, getBodyRadius()*0.4 - antennaLength, -antennaLength * 3);
        leftAntenna.setStartX(-getBodyRadius()*0.47);
        leftAntenna.setStartY(-getBodyRadius() * 0.9);
        leftAntenna.setEndX(-getBodyRadius()*0.4 - antennaLength);
        leftAntenna.setEndY(-antennaLength * 3);

        rightAntenna.setStartX(getBodyRadius()*0.47);
        rightAntenna.setStartY(-getBodyRadius() * 0.9);
        rightAntenna.setEndX(getBodyRadius()*0.4 + antennaLength);
        rightAntenna.setEndY(-antennaLength * 3);
        getChildren().addAll(leftAntenna, rightAntenna);
    }

    // Override the startMoving method to initiate forward movement
    @Override
    public void startMoving() {
        if (!isMoving) {
            isMoving = true;
            timer=null;
            moveRobotForward();
        }
    }

    // Add a method to handle the forward movement until intersection

   protected void moveRobotForward() {
        double moveDistance = this.speed; // Adjust the distance to move in each step
        // Create a new AnimationTimer each time moveRobotForward is called
        timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                if (!continueMoving) {
                    stopMoving();
                    return;
                }
                // Rotate the robot
                for (ArenaItem arenaItem : robotArena.getArenaItems()) {
                    if (arenaItem instanceof ArenaItem && !this.equals(arenaItem)) {
                        ArenaItem otherRobot =  arenaItem;
                        if (leftAntenna.isIntersecting(otherRobot.getX(), otherRobot.getY(), otherRobot.getBodyRadius()) && rightAntenna.isIntersecting(otherRobot.getX(), otherRobot.getY(), otherRobot.getBodyRadius())) {
                            // Adjust the rotation angle to avoid collision
                            rotateRightRobotCollision();
                            System.out.println("????");
                        }
                        // Check for potential collision with left antenna
                        if (leftAntenna.isIntersecting(otherRobot.getX(), otherRobot.getY(), otherRobot.getBodyRadius())) {
                            // Adjust the rotation angle to avoid collision
                            rotateRightRobotCollision();
                            System.out.println("????");
                        }

                        // Check for potential collision with right antenna
                        if (rightAntenna.isIntersecting(otherRobot.getX(), otherRobot.getY(), otherRobot.getBodyRadius())) {
                            // Adjust the rotation angle to avoid collision
                            rotateLeftRobotCollision();
                        }
                    }
                }

                // Rotate the robot
                setRotate(rotationAngle);

                // Convert rotation angle to radians
                double radianAngle = Math.toRadians(rotationAngle);

                // Calculate the movement direction based on the current rotation angle
                double deltaX = Math.cos(radianAngle) * moveDistance;
                double deltaY = Math.sin(radianAngle) * moveDistance;

                // Move the robot forward in the direction of its antennas
                updatePosition(getX() + deltaY, getY() - deltaX);

                // Move antennae along with the robot
                double antennaLength = getBodyRadius() * 0.8;
                leftAntenna.setStartY(-getBodyRadius() * 0.9);
                leftAntenna.setEndY(-antennaLength * 3);

                rightAntenna.setStartY(-getBodyRadius() * 0.9);
                rightAntenna.setEndY(-antennaLength * 3);

                // Check for intersection with the canvas border or other objects
               if (rightAntenna.isIntersectingCanvasBorder() && leftAntenna.isIntersectingCanvasBorder()) {
                    resumeMoving();
                   turn();
               }
                if (leftAntenna.isIntersectingCanvasBorder()) {
                    resumeMoving();
                    turn();
                }
                if (rightAntenna.isIntersectingCanvasBorder()) {
                    resumeMoving();
                    rotateLeft();
                }
            }
        };

        timer.start(); // Start the AnimationTimer
    }

    // Add a method to stop the forward movement
    public void stopMoving() {
        isMoving = false;

        if (timer != null) {
            timer.stop();
        }
    }

    public void resumeMoving() {
        isMoving = true;

        if (timer != null) {
            timer.start();
        }
    }

    // Add a method to rotate the robot to the left
    public void rotateLeft() {
        rotationAngle -= 5.0; // Adjust the rotation angle
    }

    // Add a method to rotate the robot to the right
    public void turn() {
        rotationAngle += 5.0; // Adjust the rotation angle
        normalizeRotationAngle();
    }
    public void rotateRightRobotCollision() {
        rotationAngle += 15.0; // Adjust the rotation angle
        normalizeRotationAngle();
    }
    public void rotateLeftRobotCollision() {
        rotationAngle -= 15.0; // Adjust the rotation angle
        normalizeRotationAngle();
    }
    private void normalizeRotationAngle() {
        if (rotationAngle < 0) {
            rotationAngle = (rotationAngle % 360) + 360;
        } else if (rotationAngle >= 360) {
            rotationAngle %= 360;
        }
        setRotate(rotationAngle);
    }

    @Override
    public String toString() {
        return String.format("RobotWithAntennae position=(%.0f, %.0f), rotationAngle=%.1f", getX(), getY(), rotationAngle);
    }









    protected void updateView() {
        setTranslateX(this.getX());
        setTranslateY(this.getY());


    }
}