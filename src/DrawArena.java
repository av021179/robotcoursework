import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.File;

public class DrawArena extends Application {
    public RobotArena robotArena = new RobotArena();
    private BorderPane borderPane = new BorderPane();
    private Text locationText = new Text();
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Simulation Application");

        // Create the main layout
        Scene scene = new Scene(borderPane, 800, 600);

        // Set the stage's scene
        primaryStage.setScene(scene);

        // Create menu bar
        MenuBar menuBar = createMenuBar(primaryStage);
        borderPane.setTop(menuBar);

        // Create toolbar
        ToolBar toolBar = createToolBar();
        borderPane.setBottom(toolBar);

        // Add location text to the top right corner
        BorderPane.setAlignment(locationText, Pos.TOP_RIGHT);
        BorderPane.setMargin(locationText, new Insets(10, 10, 10, 10));
        borderPane.setRight(locationText);

        // Create timeline for updating UI
        Timeline timeline = new Timeline(new KeyFrame(Duration.millis(100), event -> updateUI(borderPane)));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();

        // Show the stage
        primaryStage.show();
    }
    private MenuBar createMenuBar(Stage primaryStage) {
        MenuBar menuBar = new MenuBar();

        // File menu
        Menu fileMenu = new Menu("File");
        MenuItem newMenuItem = new MenuItem("New");
        MenuItem openMenuItem = new MenuItem("Open");
        MenuItem saveMenuItem = new MenuItem("Save");
        MenuItem exitMenuItem = new MenuItem("Exit");

        // Handle events for menu items
        newMenuItem.setOnAction(e -> handleNew());
        openMenuItem.setOnAction(e -> handleOpen());
        saveMenuItem.setOnAction(e -> handleSave());
        exitMenuItem.setOnAction(e -> Platform.exit());

        fileMenu.getItems().addAll(newMenuItem, openMenuItem, saveMenuItem, new SeparatorMenuItem(), exitMenuItem);

        // Edit menu
        Menu editMenu = new Menu("Edit");
        MenuItem addRobotMenuItem = new MenuItem("Add Robot");
        MenuItem removeRobotMenuItem = new MenuItem("Remove Robot");

        // Handle events for menu items

        addRobotMenuItem.setOnAction(e -> handleAddRobot());
        removeRobotMenuItem.setOnAction(e -> handleRemoveRobot());

        editMenu.getItems().addAll( addRobotMenuItem, removeRobotMenuItem);

        // Simulation menu
        Menu simulationMenu = new Menu("Simulation");
        MenuItem startMenuItem = new MenuItem("Start");
        MenuItem pauseMenuItem = new MenuItem("Pause");
        MenuItem resetMenuItem = new MenuItem("Reset");

        // Handle events for menu items
        startMenuItem.setOnAction(e -> handleStart());
        pauseMenuItem.setOnAction(e -> handlePause());


        simulationMenu.getItems().addAll(startMenuItem, pauseMenuItem, resetMenuItem);

        // Help menu
        Menu helpMenu = new Menu("Help");
        MenuItem instructionsMenuItem = new MenuItem("Instructions");
        MenuItem aboutMenuItem = new MenuItem("About");

        // Handle events for menu items
        instructionsMenuItem.setOnAction(e -> handleInstructions());
        aboutMenuItem.setOnAction(e -> handleAbout());

        helpMenu.getItems().addAll(instructionsMenuItem, aboutMenuItem);

        // Add menus to the menu bar
        menuBar.getMenus().addAll(fileMenu, editMenu, simulationMenu, helpMenu);

        return menuBar;
    }

    private ToolBar createToolBar() {
        ToolBar toolBar = new ToolBar();

        // Create toolbar buttons
        Button startButton = new Button("Start");
        Button pauseButton = new Button("Pause");
        Button addRobotButton = new Button("Robot");
        Button addAvoiderButton = new Button("Avoider");
        Button addAntennaeButton = new Button("Antennae");
        Button addObstacleButton = new Button("Obstacle");

        // Handle events for toolbar buttons
        startButton.setOnAction(e -> handleStart());
        pauseButton.setOnAction(e -> handlePause());
        addAntennaeButton.setOnAction(e->robotArena.handleAddAntennae(borderPane));
        addRobotButton.setOnAction(e->robotArena.handleAddRobot(borderPane));
        addAvoiderButton.setOnAction(e->handleAddAvoider());
        addObstacleButton.setOnAction(e->handleAddObstacle());
        // Add buttons to the toolbar
        toolBar.getItems().addAll(startButton, pauseButton, addRobotButton,addAntennaeButton,addAvoiderButton,addObstacleButton);

        return toolBar;
    }


    private void handleAddObstacle() {

    }

    private void handleAddAvoider() {
        
    }

    private void handleAddRobot() {
        
    }




    private void handleNew() {
        // Handle "New" action
        System.out.println("New Action");
    }

    private void handleOpen() {
        // Handle "Open" action
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Configuration File");
        File file = fileChooser.showOpenDialog(null);
        if (file != null) {
            System.out.println("Open Action: " + file.getAbsolutePath());
        }
    }

    private void handleSave() {
        // Handle "Save" action
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Configuration File");
        File file = fileChooser.showSaveDialog(null);
        if (file != null) {
            System.out.println("Save Action: " + file.getAbsolutePath());
        }
    }




    private void handleRemoveRobot() {
        // Handle "Remove Robot" action
        System.out.println("Remove Robot Action");
    }

    private void handleStart() {
        // Handle "Start" action
        Robot.continueMoving=true;
        for(ArenaItem robot : robotArena.arenaItems){
            robot.startMoving();
        }
        System.out.println("Start Action");
    }

    private void handlePause() {
        // Handle "Pause" action
        Robot.continueMoving=false;
        for(ArenaItem robot : robotArena.arenaItems){
            System.out.println( robot.toString());
        }
        System.out.println("Pause Action");
    }


    private void handleInstructions() {
        // Handle "Instructions" action
        System.out.println("Instructions Action");
    }

    private void handleAbout() {
        // Handle "About" action
        System.out.println("About Action");
    }
    private void updateUI(BorderPane borderPane) {
        // Create or get the existing arenaPane
        Pane arenaPane = getOrCreateArenaPane(borderPane);

        // Clear existing children
        arenaPane.getChildren().clear();

        // Add updated ArenaItems
        arenaPane.getChildren().addAll(robotArena.arenaItems);

        // Update the location text
        updateLocationText();

        // Request a layout pass
        borderPane.requestLayout();
    }

    private Pane getOrCreateArenaPane(BorderPane borderPane) {
        for (Node child : borderPane.getChildren()) {
            if (child instanceof Pane) {
                Pane existingPane = (Pane) child;
                if (existingPane.getId() != null && existingPane.getId().equals("arenaPane")) {
                    return existingPane;
                }
            }
        }

        Pane newArenaPane = new Pane();
        newArenaPane.setId("arenaPane");

        // Set the newArenaPane as the center of the BorderPane
        borderPane.setCenter(newArenaPane);

        return newArenaPane;
    }
    private void updateLocationText() {
        StringBuilder locationStringBuilder = new StringBuilder();
        for (ArenaItem robot : robotArena.arenaItems) {
            locationStringBuilder.append(robot.toString()).append("\n");
        }
        locationText.setText(locationStringBuilder.toString());
    }

}
